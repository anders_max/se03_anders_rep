import java.util.Scanner;

public class PCHaendler {
	
	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		String str = myScanner.next();
		return str;
}
	public static int liesInt(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int Int = myScanner.nextInt();
		return Int;
		
	}
	public static double liesDouble(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double Double = myScanner.nextDouble();
		return Double;
	}
	


	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		//System.out.println("was m�chten Sie bestellen?");
		//String artikel = myScanner.next();
		String artikel = liesString("was m�chten Sie bestellen?");
		
		//System.out.println("Geben Sie die Anzahl ein:");
		//int anzahl = myScanner.nextInt();
		int anzahl = liesInt("Geben Sie die Anzahl ein");

//		System.out.println("Geben Sie den Nettopreis ein:");
//		double preis = myScanner.nextDouble();
		double preis = liesDouble("Geben Sie den Nettopreis ein");

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();
		

		// Verarbeiten
		double nettogesamtpreis = anzahl * preis;
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}

