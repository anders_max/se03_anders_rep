import java.util.ArrayList;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;

	public Raumschiff() {
		this.photonentorpedoAnzahl = 0;
		this.energieversorgungInProzent = 0;
		this.schildeInProzent = 0;
		this.huelleInProzent = 0;
		this.lebenserhaltungssystemeInProzent = 0;
		this.schiffsname = "Unbekannt";
		this.androidenAnzahl = 0;
		this.ladungsverzeichnis = new ArrayList<>();
		this.broadcastKommunikator = new ArrayList<>();
	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, String schiffsname, int androidenAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.schiffsname = schiffsname;
		this.androidenAnzahl = androidenAnzahl;
		this.ladungsverzeichnis = new ArrayList<>();
		this.broadcastKommunikator = new ArrayList<>();
	}

	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return this.schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}

	public void photonentorpedosSchiessen(Raumschiff r) {

		if (this.photonentorpedoAnzahl < 1) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			this.photonentorpedoAnzahl--;
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			treffer(r);
		}
	}

	public void phaserkanoneSchiessen(Raumschiff r) {
		if (this.energieversorgungInProzent < 50) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			this.energieversorgungInProzent = this.energieversorgungInProzent - 50;
			nachrichtAnAlle("Phaserkanone abgeschossen");
			treffer(r);
		}
	}

	private void treffer(Raumschiff r) {
		System.out.println(r.schiffsname + "wurde getroffen!");
		r.schildeInProzent = r.schildeInProzent - 50;
		if (r.schildeInProzent <= 0) {
			r.huelleInProzent = r.huelleInProzent - 50;
			r.energieversorgungInProzent = r.energieversorgungInProzent - 50;
		}
		if (r.huelleInProzent <= 0) {
			r.lebenserhaltungssystemeInProzent = 0;
			nachrichtAnAlle("Lebenserhaltungssysteme wurden vernichtet!");
		}
	}

	public void nachrichtAnAlle(String message) {
		broadcastKommunikator.add(message);
		System.out.println(message);
	}

	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return broadcastKommunikator;
	}

	public void photonentorpedosLaden(int anzahlTorpedos) {
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			Ladung ladung = ladungsverzeichnis.get(i);
			if (ladung.getBezeichnung().equals("Photonentorpedo")) {
				if (anzahlTorpedos > ladung.getMenge()) {
					this.photonentorpedoAnzahl = ladung.getMenge();
					System.out.println(ladung.getMenge() + "Photonentorpedo(s) eingesetzt");
					ladung.setMenge(0);
				} else {
					ladung.setMenge(ladung.getMenge() - anzahlTorpedos);
					this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl() + anzahlTorpedos);
					System.out.println(anzahlTorpedos + "Photonentorpedo(s) eingesetzt");
				}

				return;
			}
		}
		System.out.println("Keine Photonentorpedos gefunden!");
		this.nachrichtAnAlle("-=*Click*=-");

	}

	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlDroiden) {
		int zufallszahl = (int) (Math.random() * 100);
		int einzusetzendeAndroiden = 0;
		if (anzahlDroiden > this.androidenAnzahl) {
			einzusetzendeAndroiden = this.androidenAnzahl;
		} else {
			einzusetzendeAndroiden = anzahlDroiden;
		}
		int anzahlStrukturen = 0;
		if (schutzschilde) {
			anzahlStrukturen++;
		}
		if (energieversorgung) {
			anzahlStrukturen++;
		}
		if (schiffshuelle) {
			anzahlStrukturen++;
		}

		int reparaturProzente = (zufallszahl * einzusetzendeAndroiden) / anzahlStrukturen;
		if (schutzschilde) {
			this.setSchildeInProzent(getSchildeInProzent() + reparaturProzente);
			;
		}
		if (energieversorgung) {
			this.setEnergieversorgungInProzent(getEnergieversorgungInProzent() + reparaturProzente);
			;
		}
		if (schiffshuelle) {
			this.setHuelleInProzent(getHuelleInProzent() + reparaturProzente);
		}
	}

	public void zustandRaumschiff() {
		System.out.println("Schiffsname: " + this.schiffsname);
		System.out.println("Photonentorpedos: " + this.photonentorpedoAnzahl);
		System.out.println("Energie:" + this.energieversorgungInProzent);
		System.out.println("Schilde: " + this.schildeInProzent);
		System.out.println("H�lle: " + this.huelleInProzent);
		System.out.println("Lebenserhaltungssysteme: " + this.lebenserhaltungssystemeInProzent);
		System.out.println("Androiden:" + this.androidenAnzahl + "\n");		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			System.out.println(ladungsverzeichnis.get(i));
		}

	}

	public void ladungsverzeichnisAufraumen() {
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			Ladung ladung = ladungsverzeichnis.get(i);
			if (ladung.getMenge() == 0) {
				ladungsverzeichnis.remove(i);
				i--;
			}
		}

	}
}
