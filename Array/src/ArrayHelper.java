
public class ArrayHelper {

	public static void main(String[] args) {

		int[] zahlen = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		convertArrayToString(zahlen);
		System.out.println(convertArrayToString(zahlen));		
	}

	public static String convertArrayToString(int[] zahlen) {

		String zeichenkette = "[ ";
		for (int i = 0; i < zahlen.length; i++) {
			zeichenkette = zeichenkette + zahlen[i] + " ";
		}
		zeichenkette = zeichenkette + "]";
		return zeichenkette;
	}
}