import java.util.Scanner;

public class Array {

	public static void main(String[] args) {

		 //zahlen();
		 //ungeradeZahlen();
		 //palindrom();
		 lotto();
	
	}
	
	public static void zahlen() {

		int[] zahlenkette = new int[10];

		for (int i = 0; i < zahlenkette.length; i++) {
			zahlenkette[i] = i;
			System.out.print(zahlenkette[i] + " ");
		}
	}
	
	public static void ungeradeZahlen() {
		int[] zahlenkette = new int[10];
		for (int i = 0; i < zahlenkette.length; i++) {
			zahlenkette[i] = i * 2 + 1;
			System.out.print(zahlenkette[i] + " ");
		}
	}

	public static void palindrom() {
		Scanner myScanner = new Scanner(System.in);
		char[] buchstabenkette = new char[5];

		for (int i = 0; i < buchstabenkette.length; i++) {
			int a = i + 1;
			System.out.println("Bitte geben Sie das " + a + ". Zeichen ein");
			buchstabenkette[i] = myScanner.next().charAt(0);
			System.out.println(buchstabenkette[i]);
		}
		for (int i = buchstabenkette.length - 1; i >= 0; i--) {
			System.out.print(buchstabenkette[i]);
		}
		myScanner.close();

	}

	public static void lotto() {
		int[] lottozahlen = { 3, 7, 12, 18, 37, 42 };
		for (int i = 0; i < lottozahlen.length; i++) {
			System.out.println(lottozahlen[i] + " ");
		}
	
		boolean imArray = false;
		int zahl = 12;

		for (int i = 0; i < lottozahlen.length; i++) {
			if (lottozahlen[i] == zahl) {
				imArray = true;
				break;
			}
		}
		if (imArray == true) {
			System.out.println("Die Zahl 12 ist in der Ziehung enthalten");
		} else {
			System.out.println("Die Zahl 12 ist nicht in der Ziehung enthalten");
		}

		imArray = false;
		zahl = 13;
		for (int i = 0; i < lottozahlen.length; i++) {
			if (lottozahlen[i] == zahl) {
				imArray = true;
				break;
			}
		}
		if (imArray == true) {
			System.out.println("Die Zahl 13 ist in der Ziehung enthalten");
		} else {
			System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten");
		}
	}
}
