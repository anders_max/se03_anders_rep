
public class ArrayReverse {

	public static void main(String[] args) {

		int[] zahlen = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		//zahlenUmdrehen(zahlen);
		zahlenUmdrehen2(zahlen);
	}

	public static void zahlenUmdrehen(int[] zahlen) {

		int zwischenspeicher = 0;
		for (int i = 0; i <= ((zahlen.length - 1) / 2); i++) {
			zwischenspeicher = zahlen[i];
			zahlen[i] = zahlen[(zahlen.length - 1 - i)];
			zahlen[zahlen.length - 1 - i] = zwischenspeicher;
		}
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print(zahlen[i] + " ");
		}
	}

	public static int[] zahlenUmdrehen2(int[] zahlen) {

		int[] zahlen2 = new int[10];
		int zwischenspeicher = 0;
		for (int i = 0; i <= ((zahlen.length - 1) / 2); i++) {
			zwischenspeicher = zahlen[i];
			zahlen2[i] = zahlen[(zahlen.length - 1 - i)];
			zahlen2[zahlen.length - 1 - i] = zwischenspeicher;
		}
		for (int i = 0; i < zahlen2.length; i++) {
			System.out.print(zahlen2[i] + " ");
		}

		return zahlen2;
	}
}
