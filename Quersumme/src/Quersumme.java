import java.util.Scanner;

public class Quersumme {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
	
		
		printQuersumme("Geben Sie eine Zahl ein: ", tastatur);
	}
		
		
		public static void printQuersumme(String text, Scanner myScanner) {
			System.out.println(text);
			int zahl = myScanner.nextInt();
			int quersumme = 0;
			while (zahl!= 0) {
			quersumme = quersumme +(zahl % 10);
			zahl = zahl / 10;
			}
			System.out.println(quersumme);
	}

}
