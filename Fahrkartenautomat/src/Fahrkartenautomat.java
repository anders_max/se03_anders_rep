import java.util.Scanner;

class Fahrkartenautomat {

	public static double fahrkartenbestellungErfassen() {
		int anzahlTickets;
		int eingabe;
		double ticketPreis = 0;
		double ticketPreis1 = 2.9;
		double ticketPreis2 = 8.6;
		double ticketPreis3 = 23.5;
		Scanner tastatur = new Scanner(System.in);
		System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\n" + "Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n" 
				+ "Tageskarte Regeltarif AB [8,60 EUR] (2)\n" + "Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n" + "Ihre Wahl:");
			
		eingabe = tastatur.nextInt();
		while (eingabe < 1 || eingabe > 3) {
			
			System.out.println(">>falsche Eingabe<<");
			eingabe = tastatur.nextInt();
		}

		if (eingabe == 1) {
			ticketPreis = ticketPreis1;
		}
		if (eingabe == 2) {
			ticketPreis = ticketPreis2;
		}
		if (eingabe == 3) {
			ticketPreis = ticketPreis3;
		}
		
		
		System.out.print("Anzahl der Tickets: ");
		anzahlTickets = tastatur.nextInt();
		
		return ticketPreis * anzahlTickets;
		

	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingeworfenemuenze;
		double eingezahlterGesamtbetrag = 0.0;
		Scanner tastatur = new Scanner(System.in);

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.format("Noch zu zahlen: %4.2f �� %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfenemuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfenemuenze;
			
		}
		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double rueckgabebetrag) {

		if (rueckgabebetrag > 0.0) {
			System.out.format("Der R�ckgabebetrag in H�he von %4.2f �� %n", rueckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) {
				System.out.println("2 EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) {
				System.out.println("1 EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) {
				System.out.println("50 CENT");
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) {
				System.out.println("20 CENT");
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) {
				System.out.println("10 CENT");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05) {
				System.out.println("5 CENT");
				rueckgabebetrag -= 0.05;
			}
		}
	}

	public static void main(String[] args) {

		while (0 != 1) {
			double zuZahlenderBetrag;
			double rueckgabebetrag;

			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(rueckgabebetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.\n" + " ");
		}
	}

}