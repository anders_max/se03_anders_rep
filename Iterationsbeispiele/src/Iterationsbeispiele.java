import java.util.Scanner;

public class Iterationsbeispiele {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);

		//printNNumbers("Geben Sie eine Zahl ein: ", tastatur);
		//printNNumbers2("Geben Sie eine Zahl ein: ", tastatur);
		//printFakultät("Geben Sie eine Zahl ein: ", tastatur);
		printQuersumme("Geben Sie eine Zahl ein: ", tastatur);
	}

	public static void printNNumbers(String text, Scanner myScanner) {
		System.out.println(text);
		int n = myScanner.nextInt();

		int zaehler = 1;
		while (zaehler <= n) {
			if (zaehler == n) {
				System.out.print(zaehler);
			} else {
				System.out.print(zaehler + ",");
			}
			zaehler++;
		}
	}

	public static void printNNumbers2(String text, Scanner myScanner) {
		System.out.println(text);
		int n = myScanner.nextInt();

		int zaehler = n;
		while (zaehler >= 1) {
			if (zaehler == 1) {
				System.out.print(zaehler);
			} else {
				System.out.print(zaehler + ",");
			}
			zaehler--;
		}
	}

	public static void printFakultät(String text, Scanner myScanner) {
		System.out.println(text);
		int n = myScanner.nextInt();
		int zaehler = 1;
		int ergebnis = 1;
		while (zaehler <= n) {
			ergebnis = ergebnis * zaehler;
			zaehler++;
			
			
			
		}
		if(n == 0) {
			System.out.println(n + "! = 1");
		}else {
		System.out.println(n + "! = " +ergebnis);
		}
	}
	public static void printQuersumme(String text, Scanner myScanner) {
		int n = myScanner.nextInt();
		int ergebnis = 0;
		while(n != 0) {
			ergebnis = ergebnis + (n % 10);
		}
	}
	
}