import java.util.Scanner;

public class Programmier�bungen {
	
	public static String liesString(String text) {
		Scanner myScanner = new Scanner (System.in);
		System.out.println(text);
		String str = myScanner.next();
		return str;
	}
		
		
	public static int liesInt(String text) {
		Scanner myScanner = new Scanner (System.in);
		System.out.println(text);
		int int1 = myScanner.nextInt();
		return int1;
		}
	
	
	public static double liesdouble(String text) {
		Scanner myScanner = new Scanner (System.in);
		System.out.println(text);
		double double1 = myScanner.nextDouble();
		return double1;
	}

	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double gesamtBruttopreis = nettogesamtpreis * (1 + mwst / 100);
		return gesamtBruttopreis;
	}
	
	public static void rechnungAusgeben(String artikel, int anzahl, double nettoGesamtpreis, double bruttoGesamtpreis, double mwst) { 
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettoGesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttoGesamtpreis, mwst, "%");
	}
	
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		//System.out.println("was m�chten Sie bestellen?");
		//String artikel = myScanner.next();
		String artikel = liesString("Was möchten sie bestellen?");
		
		//System.out.println("Geben Sie die Anzahl ein:");
		//int anzahl = myScanner.nextInt();
		int anzahl = liesInt("Geben sie die Anzahl ein: ");

		//System.out.println("Geben Sie den Nettopreis ein:");
		//double preis = myScanner.nextDouble();
		double preis = liesdouble("Geben Sie den Nettopreis ein: ");

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();

		// Verarbeiten
		//double nettogesamtpreis = anzahl * preis;
		//double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		double nettoGesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttoGesamtpreis = berechneGesamtbruttopreis(nettoGesamtpreis, mwst);
		
		// Ausgeben
		rechnungAusgeben(artikel, anzahl, nettoGesamtpreis, bruttoGesamtpreis, mwst);
		//System.out.println("\tRechnung");
		//System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		//System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}

}
