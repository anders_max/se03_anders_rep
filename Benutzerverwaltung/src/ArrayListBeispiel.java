import java.util.ArrayList;

public class ArrayListBeispiel {

	public static void main(String[] args) {
		
		String[] sliste = new String[2];

		sliste[0]="Max";
		sliste[1]="Anna";
		
		ArrayList<String> strList = new ArrayList<String>();
		
		strList.add("Max");
		strList.add("Anna");
		strList.add("Klaus");
		
		System.out.println(strList);
		
		strList.remove("Anna");
		System.out.println(strList);

	}

}
