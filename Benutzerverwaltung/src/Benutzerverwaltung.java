import java.util.ArrayList;

import java.util.Scanner;

public class Benutzerverwaltung {

	private static ArrayList<Benutzer> benutzerliste = new ArrayList<Benutzer>();

	public static void main(String[] args) {

		int auswahl;

		do {
			auswahl = menu();
			switch (auswahl) {
			case 1:
				benutzerAnzeigen();
				break;
			case 2:
				benutzerErfassen();
				break;
			case 3:
				benutzerLöschen();
				break;
			case 4:
				System.exit(0);
			default:
				System.err.println("\nFalsche Eingabe.\n");
			}
		} while (true);

	}

	public static void benutzerErfassen() {
		String name;
		int bnr;
		Scanner input = new Scanner(System.in);
		System.out.println("Name eingeben:");
		name = input.nextLine();
		System.out.println("Benutzernummer eingeben:");
		bnr = input.nextInt();
		Benutzer b1 = new Benutzer(name, bnr);
		benutzerliste.add(b1);
		input.close();
	}

	public static void benutzerAnzeigen() {

		for (int i = 0; i < benutzerliste.size(); i++) {
			System.out.println(benutzerliste.get(i));
		}

	}

	public static void benutzerLöschen() {
		benutzerliste.clear();
	}

	public static int menu() {

		int selection;
		Scanner input = new Scanner(System.in);

		/***************************************************/

		System.out.println("     Benutzerverwaltung     ");
		System.out.println("----------------------------\n");
		System.out.println("1 - Benutzer anzeigen");
		System.out.println("2 - Benutzer erfassen");
		System.out.println("3 - Benutzer löschen");
		System.out.println("4 - Ende");

		System.out.print("\nEingabe:  ");
		selection = input.nextInt();
		input.close();
		return selection;
	}

}
