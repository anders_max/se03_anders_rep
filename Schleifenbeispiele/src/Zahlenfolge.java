import java.util.Scanner;

public class Zahlenfolge {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte Zahl eingeben:");
		int n = myScanner.nextInt();

		int summe = 0;
		for (int i = 0; i <= n; i++) {
			summe = summe + i;
		}
		System.out.println("Die Summe f�r A betr�gt: " + summe);

		int summe1 = 0;
		for (int i = 0; i <= 2 * n; i = i + 2) {
			summe1 = summe1 + i;
		}
		System.out.println("Die Summe f�r B betr�gt: " + summe1);

		int summe2 = 0;
		for (int i = 1; i <= 2 * n + 1; i = i + 2) {
			summe2 = summe2 + i;
		}
		System.out.println("Die Summe f�r C betr�gt: " + summe2);

		myScanner.close();
	}
}
