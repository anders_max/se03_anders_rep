import java.util.Scanner;


public class Temperaturberechnung {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Bitte den Startwert in Celsius eingeben:");
		int startwert = myScanner.nextInt();
		System.out.println("Bitte den Endwert in Celsius eingeben:");
		int endwert = myScanner.nextInt();
		System.out.println("Bitte den Schrittweite eingeben:");
		int schrittweite = myScanner.nextInt();
		
		while(startwert <= endwert) {
			double fahrenheit = 32+(startwert*1.8);
			System.out.println(startwert + "�C" +"			"+ fahrenheit + "�F");
			startwert = startwert + schrittweite;
		}
		
		myScanner.close();
		
	}

}
